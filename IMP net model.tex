\documentclass[]{article}
\usepackage{amsmath}
\usepackage{graphicx,tabularx,hyperref}
%\usepackage{subfig}
\usepackage{tikz}
\usetikzlibrary{arrows}
\usepackage{verbatim}
\usepackage{subcaption}
\usepackage[labelformat=parens,labelsep=quad,skip=3pt]{caption}
\usepackage{graphicx}
\usepackage{ifthen}

\newboolean{shownotes}
\setboolean{shownotes}{true}


\ifthenelse{\boolean{shownotes}}
{
\newcommand{\notes}[1]{[\textcolor{red}{\emph{#1}}]}
}
{
\newcommand{\notes}[1]{}
}

\newcommand{\E}{\mathrm{e}}
\newcommand{\D}{\mathrm{d}}

\bibliographystyle{siam}

%opening
\title{An Integrated Projection Model for Projecting Bednet Proportional Hole Index, Coverage and Use}
\author{Joshua O. Yukich}

\begin{document}

\maketitle

\begin{abstract}
	
\end{abstract}

\section{Introduction}

Insecticide treated bednets are one of the most successful public health interventions developed in the past 40 years. They have been responsible for prevention of significant mortality and morbidity globally, but especially in sub-Saharan Africa. These products, however, have limited and uncertain lifetimes which vary dramatically based on product specifications, as well as local environmental factors and individual and household behaviors and characteristics of net owners.  While manufacturers and pre-qualification process have classically assumed that the life of these products is consistent with an approximate 3 year half life. Considerable evidence shows that their lifetimes tend to be much shorter, but also may vary significantly from place to place. In order to better plan for the deployment of programs dependent on these products as well as to understand the likely population level characteristics of a group of nets deployed over time we have developed and integrated projection model to describe the survival, recruitment and physical decay of nets over time. Such results can also be useful in when integrated into transmission dynamic models of disease in which the efficacy of nets and use over time may depend on their physical state as well as their retention and use.

\section{Methods}
The lifecycle of a bednet is shown in Figure~\ref{fig:lifecycles}. This figures illustrates both lifecycles in which all new net additions are exogenous to the system and another in which the replacement of nets is determined as $1-survivorship$ such that all net attrition is compensated in the same time step by replacement leading to an equilibrium net population.


\begin{figure}
	\begin{subfigure}{12cm}
		\centering
		\caption{Only exogenous distribution}
\tikzstyle{int}=[draw, fill=blue!20, minimum size=2em]
\tikzstyle{init} = [pin edge={to-,thin,black}]

\begin{tikzpicture}[node distance=2.5cm,auto,>=latex']
\node [int] (a) {$n(\phi,t)$};
\node [int] (c) [right of=a] {$n(\phi, t+1)$};
\node [int] (d) [right of=c, node distance=4cm] {$n(\phi',t+1)$};
\node [coordinate] (end) [right of=c, node distance=2cm]{};
\node [int] (new) [above of=d, node distance=2cm]{$e(\phi,t+1)$};
\path[->] (new) edge node {$C(\phi',\phi)$} (d);
\path[->] (a) edge node {$s(\phi)$} (c);
\draw[->] (c) edge node {$G(\phi',\phi)$} (d) ;
\end{tikzpicture}

\vspace{1cm}
	\end{subfigure}
	\begin{subfigure}{12cm}
		\centering
		\caption{Equilibrium distribution}
\tikzstyle{int}=[draw, fill=blue!20, minimum size=2em]
\tikzstyle{init} = [pin edge={to-,thin,black}]

\begin{tikzpicture}[node distance=2.5cm,auto,>=latex']
\node [int] (a) {$n(\phi,t)$};
\node [int] (c) [right of=a] {$n(\phi, t+1)$};
\node [int] (d) [right of=c, node distance=4cm] {$n(\phi',t+1)$};
\node [int] (e) [above of=c, node distance=2cm]{$e(\phi,t+1)$};
\node [coordinate] (end) [right of=c, node distance=2cm]{};
%\node [coordinate] (new) [above of=d, node distance=1cm]{};
\path[->] (a) edge node {$(1-s(\phi))$} (e);
\path[->] (e) edge node {$C(\phi',\phi)$} (d);
\path[->] (a) edge node {$s(\phi)$} (c);
\draw[->] (c) edge node {$G(\phi',\phi)$} (d) ;
\end{tikzpicture}

	\end{subfigure}
	\caption{Life cycle figures of bednet ownership and physical integrity}
	\label{fig:lifecycles}
\end{figure}





Model development follows guidance on construction of integrated projection models as per Rees \textit{et al} \cite{rees:ipmuserguide}. Assuming that bednets start in a optimal condition, the number of bednets remaining in a given population at time $t$ after their distribution at $t = 0$ is defined as per Equation~\ref{ipm_eqn}.

\begin{equation} \label{ipm_eqn}
N(t) = \int_{0}^{\infty}n(\phi,t)d\phi
\end{equation}

Where $\phi$ is the proportional hole index. From time $t$ to $t + 1$ it is possible for the bednet to either increase, decrease, or remain the same in terms of $\phi$. The net may also fail to survive, or attrit over the same interval in a process that is dependent on its $\phi$. The function $n(\phi,t)$ describes the probability density for a net to have a given $\phi$ at time $t$. In other words, $n(\phi,t)$ describes expectation of the physical deterioration of the net.

The number of individuals present with a given $\phi$ at time $t$ can be described as approximately $n(\phi, t)h$ where $h$ is size of a category of PHI. Such that we are interested in the number of individuals in the class $[\phi, \phi+h]$. \notes{Nakul: I struggle to understand this sentence. What exactly does $h$ mean? Why do you multiple $n(\phi,t)$ by $h$?}

To describe the result of a combination of physical deterioration and attrition we describe the function $P(\phi',\phi)$, representing survival and change in $\phi$, and $F(t, \phi)$ representing the distribution of new nets. In this case $\phi'$ represents $\phi$ at time $t + 1$. Because new net distribution is entirely exogenous to the system  and new nets are always distributed in an intact condition, we consider introduction of new nets to be a function of time and $\phi$, where $F(t,\phi) = 0$ if $\phi \neq 0$. \notes{This implies that $F(t, \phi)$ is not a continuous function of $\phi$, but as long as you're integrating with respect to $\phi$ and not taking the derivative, this is fine --- but should be noted.}

For an net with PHI of $\phi$ at time $t$, $P(\phi',\phi)h$ is the probability that the survives (does not attrit) at or before time $t + 1$, and that its PHI is in the interval $[\phi',\phi'+ h]$ at time $t + 1$. \notes{I also don't understand the notation with $h$ here.}  We can decompose $P$ into a function of PHI dependent survival and degradation as follows:

\begin{equation} \label{P_eq}
P(\phi',\phi) = s(\phi)G(\phi',\phi)
\end{equation}

Where $s(\phi)$ is the survival probability and $G(\phi', \phi)$ is the expectation of PHI change.

These functions can be combined to describe change in survival and change in PHI over time as:

\begin{equation} \label{kern_eq}
K(\phi', \phi, t) = P(\phi', \phi) + F(t, \phi)
\end{equation}
\notes{This equation doesn't look correct because $P$ should be multiplied by $n$ but $F$ should not be multiplied by $n$.}
which is a survival and deterioration kernel which can be used to determine the expectation of the population size at time $t + 1$ as follows:

\begin{equation} \label{Proj_eq}
N(t+1) = n(\phi, t+1) = \int_{0}^{\infty}K(\phi',\phi, t)n(\phi,t)d\phi
\end{equation}
\notes{The term between the equal to signs ($n$) should have an integral around it. The right hand side should separate $K$ into $P$ and $F$ and $F$ should either be multiplied by $C$ or should be a function of $(t+1)$: $n(t+1) = P\cdot n(t) + C\cdot F(t)$. It's also not clear to me why you use $C$ instead of simply using $P$ as before. Then you could say that $n(t+1) = P(n(t)+F(t))$. (Omitting $\phi$ from the notation.) What do you think?}


Since we consider the population model to be one with a census conducted post net distribution in any given period, we can there for write the full Kernel as follows:

\begin{equation} \label{Proj_eq2}
N(t+1) = n(\phi, t+1) = \int_{0}^{\infty}K(\phi',\phi, t)n(\phi,t)d\phi
\end{equation}

\subsection{Survival and recruitment}

Net survival can be described as the proportion of nets surviving from $t$ to $t + 1$, $sN(t)$ where $s$ is the survival probability. Since nets are not born but only exogenously introduced they are a function only described as a function of the proportionate hole index ($\phi$) such that the probability of survival for a given $\phi$ is $s(\phi)$ and here is modeled as the logistic function on the interval $[0,1]$, such that survival probability is given by:

\begin{equation} \label{surv_eq}
s(\phi) = \dfrac{1}{1+e^{-k(\phi-\phi_{0})}}
\end{equation}

Because nets are not assumed to reproduce, in the general case recruitment or fecundity is treated as completely exogenous and new nets are introduced not as part of the kernel but rather just before each census then projected as any other net considered in the lowest PHI class. This assumption is consistent with the new nets being introduced just before the population census at $(t+1)$. Where multiple types of nets are distributed in parallel, each ``cohort'' of nets can be projected independently and the resulting projection matrices summed by element after appropriate alignment. The latter approach allows for the joint projection of nets with differing survival and PHI growth kernels.

Growth in PHI ($\phi$) is assumed to be log-normally distributed, meaning that change in PHI from $t$ to $t + 1$ could be simulated for a specific net with a draw from a log-normal distribution with a specified mean and variance. For the purpose of developing a kernel for growth the expected density is given by the probability density function of the log-normal distribution for a change from $\phi$ to $\phi'$ (or $\Delta\phi$):

\begin{equation} \label{PDF_lognorm}
G(\phi', \phi) = G(\Delta\phi) = \dfrac{1}{\Delta\phi\sigma\sqrt{2\pi}}e^{-\dfrac{(\ln \Delta\phi - \mu)^{2}}{2\sigma^{2}}}
\end{equation}

In the basic model formulation no net repair is assumed and as such a log-normal distribution limits the movement in PHI to be always positive.  Thus the kernel for any projection $G(\phi', \phi)$ which is zero or negative in terms of $\Delta\phi$ is, by definition, $0$. This constraint could be relaxed and the probability of PHI ($\phi$) dependent repair incorporated by choosing a model for $G(\Delta\phi)$ which allows for negative outcomes, such as a normal distribution as well as changing the limits of integration in equations~\ref{ipm_eqn},~\ref{Proj_eq}, and ~\ref{Proj_eq2}. The parameters $\mu$ and $\sigma$ are predefined in the kernel or can be estimated from data.

 \subsection{Net distribution to replace all failing nets}

Assessment of the requirements to maintain full coverage of LLIN stably over time can be made by assuming that immigration into the net population is exactly the inverse of loss of nets due to attrition events. We can then rewrite the kernel function as:

 \begin{equation} \label{equil_Kern}
 K(\phi',\phi) = s(\phi)G(\phi',\phi) + [(1-s(\phi))C(\phi',\phi)]
 \end{equation}
\notes{Same issues as above for the definition of $K$.}
where the function $C(\phi',\phi)$ has normalized unit density when $\phi' = 0$ and zero density otherwise. In other words, all newly introduced nets are assigned to a $\phi$ of zero. This formulation will maintain a stable population and comparisons can therefore be used to calculate the number of distribution events and changes in the stable PHI structure of the population as net degradation and attrition parameters are altered. It assumes a fully continuous distribution of nets in which new distribution can be at least annually matched to the attrition of nets.



\subsection{Implementation of the IPM as an individual net based micro-simulation}

The

\subsection{Parameterization of the model}
A dataset on net durability derived from a net durability study conducted in Ethiopia was used to parameterize the model.
\section{Results}

Since in the basic model new nets are exogenously introduced, the stable population size is zero, and the long term stable population growth rate is zero. Calculation of the growth and degradation kernel following the midpoint rule for numeric integration and estimating the eigenvalues of the kernel matrix, at any set of sensible parameters show that, sensibly and trivially, the only equilibrium in the model is the stable, trivial equilibrium at zero. Because the long term expectation is that all nets will fails, it is much more interesting to

	
\bibliography{IPMnets}
\end{document}
	